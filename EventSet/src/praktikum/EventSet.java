package praktikum;

/*
 * In dieser Aufgabe geht es um die Klasse EventSet, deren Attribut ein boolesches Feld ist, 
 * dessen Länge im Konstruktor angegeben wird. 
 * Jedes Feldelement kann durch die Methode set auf true oder false gesetzt werden. 
 * Ferner gibt es die Methoden waitAND und waitOR, mit denen ein Thread darauf warten kann, 
 * bis alle Feldelemente true sind bzw. bis mindestens ein Feldelement true ist. 
 * Die Methoden waitAND und waitOR verändern das Feld nicht.
 * 
 * Realisieren Sie die Klasse EventSet mit Hilfe der Java-Synchronisations­konzepte synchronized, 
 * wait, notify bzw. notifyAll und schreiben Sie ein Programm dazu, in dem Sie ein Objekt der Klasse EventSet 
 * von mehreren Threads gleichzeitig benutzen, um die Klasse EventSet auszuprobieren!
 */

public class EventSet {
	private boolean[] array;
	
	public EventSet(int length){	
		if (length <= 0){
			throw new IllegalArgumentException();
		}
		this.array = new boolean[length];
	}
	
	public synchronized void set(int i, boolean b)
	{
		if ((i >= this.array.length) || i < 0){
			throw new IllegalArgumentException();
		}
		
		this.array[i] = b;
		System.out.println(i + " set to " + b);
		
		this.notifyAll();
	}
	
	private boolean CheckAND()
	{
		for (boolean element : this.array) 
		{
			if(!element)
			{
				System.out.println("CheckAND == false");
				return false;
			}
		}
		return true;
	}
	
	private boolean CheckOR()
	{
		for (boolean element : this.array) 
		{
			if(element)
			{
				return true;
			}
		}
		System.out.println("CheckOR == false");
		return false;
	}
	
	public synchronized void waitAND()
	{
		while(!this.CheckAND())
		{
			try
			{
				this.wait();
			}
			catch (InterruptedException iex){};
		}
		System.out.println(Thread.currentThread().getName() + " all true!");
	}
	
	public synchronized void waitOR()
	{
		while(!this.CheckOR())
		{
			try
			{
				this.wait();
			}
			catch (InterruptedException iex){};
		}
		System.out.println(Thread.currentThread().getName() + " one true!");
	}
}
