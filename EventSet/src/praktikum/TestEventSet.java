package praktikum;

public class TestEventSet {

	public static void main(String[] args) 
	{
		EventSet set = new EventSet(5);
		
		new Thread(()-> set.waitAND()).start();
		new Thread(()-> set.waitOR()).start();
		
		delay();
		set.set(0,true);
		// Einer ist TRUE!
		
		delay();
		set.set(0,false);
		// Alle sind false!
		
		new Thread(()-> set.waitOR()).start();
		
		delay();
		set.set(1,true);
		// Einer ist TRUE!
		
		delay();
		set.set(2,true);
		delay();
		set.set(0,true);
		delay();
		set.set(3,true);	
		delay();
		set.set(4,true);
		// Alle sind true!
	}
	
	private static void delay()
	{
		try {
			Thread.sleep(750);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
