package praktikum;

	/* In dieser Aufgabe sollen Sie in durch Implementierung der Klasse BoundedCounter 
	 * (auf deutsch: begrenzter Z�hler) das Umgehen mit synchronized, wait, notify bzw. notifyAll weiter �ben. 
	 * Die Klasse BoundedCounter soll einen int-Z�hler repr�sentieren, der durch die Methode up um eins erh�ht 
	 * und durch die Methode down um eins erniedrigt werden kann. 
	 * Im einzigen Konstruktor dieser Klasse soll ein Minimum- und ein Maximumwert f�r den int-Z�hler angegeben 
	 * werden k�nnen (d.h. der Z�hler soll nie kleiner als das im Konstruktor angegebene Minimum 
	 * und nie gr��er als das im Konstruktor angegebene Maximum werden). 
	 * Versucht ein Thread die Methode down auszuf�hren, wenn der Z�hler seinen Minimumwert besitzt, 
	 * so soll er solange verz�gert werden, bis das Erniedrigen des Z�hlers m�glich ist, ohne dass der Minimal-
	 * wert unterschritten wird (d.h. bis ein anderer Thread den Z�hler entsprechend erh�ht hat). 
	 * Das Entsprechende gilt f�r das Erh�hen des Z�hlers in der Methode up und den Maximumwert. 
	 * Zus�tzlich soll es eine Funktion get geben, die den aktuellen Z�hlerwert zur�ckliefert.
	 * 
	 * a) Geben Sie eine Implementierung der Klasse BoundedCounter an! 
	 * Der Anfangswert des Z�hlers soll der angegebene Minimalwert sein.
	 * 
	 * b) Schreiben Sie ein Programm, in dem ein Objekt der Klasse BoundedCounter von mehreren Threads 
	 * gleichzeitig benutzt wird!
	 */

public class BoundedCounter implements Runnable
{	
	private int minimum, maximum;
	private int counter;
	
	public synchronized int getCounter(){
		return this.counter;
	}
	
	public BoundedCounter(int min, int max){	
		if(min >= max){
			throw new IllegalArgumentException();
		}
		this.counter = min; // Set counter to minimum value
		this.minimum = min;
		this.maximum = max;
	}
	
	public synchronized void up(){
		while(this.counter == this.maximum)
		{
			try
			{
				this.wait();
			} 
			catch (InterruptedException iex){};
		}
		
		this.counter++;
		System.out.println(Thread.currentThread().getName() 
				+ ":Up; Counter: " + counter);

		this.notify();
	}
	
	public synchronized void down(){
		while(this.counter == this.minimum)
		{
			try
			{
				this.wait();
			}
			catch (InterruptedException iex){};
		}
		this.counter--;
		System.out.println(Thread.currentThread().getName() 
				+ ":Down; Counter: " + counter);
		
		this.notify();
	}	
	
	@Override
	public void run()
	{
		if(Math.random()>= 0.5){				
			this.up();
		} else{
			this.down();
		}
	}
}


