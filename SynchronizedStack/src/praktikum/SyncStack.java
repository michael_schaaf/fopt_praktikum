package praktikum;

import java.util.ArrayList;
import java.util.List;

/*
Aufgabe: Synchronisierter Keller

Gegeben sei die wohlbekannte Datenstruktur eines Kellers (Stack) mit den Methoden push und pop. 
Im Folgenden sollen diese Methoden so ge�ndert werden, dass die Methode push (Ablegen eines Elements im Keller) 
wie bisher auch ohne besondere Einschr�nkungen ausgef�hrt werden kann. 
Beim Aufruf der Methode pop (Entfernen des obersten Elements des Kellers und R�ckgabe des entfernten Elements als R�ckgabewert) 
soll allerdings bei leerem Keller so lange gewartet werden, bis ein Element zur�ckgegeben werden kann. 
Das hei�t: Damit kann es nicht mehr vorkommen, dass die Methode pop kein Element zur�ckgibt, da der Keller leer war. 
Die Elemente, die im Keller abgelegt werden, sollen Objekte jeder beliebigen Klasse sein k�nnen. 
Deshalb soll als Datentyp f�r die abzulegenden Elemente die Klasse Object verwendet werden. 
Der Keller soll durch ein Feld (in diesem Fall ein Object-Feld) oder eine ArrayList realisiert werden.

Implementieren Sie die Klasse SynchStack mit Hilfe der Java-Synchronisations�konzepte synchronized, wait, notify bzw. notifyAll
und schreiben Sie ein Hauptprogramm dazu, mit dem Sie Ihren synchronisierenden Stack ausprobieren k�nnen!
*/

public class SyncStack implements Runnable
{
	private int counter;
	private List<Object> list;
	
	public SyncStack(){
		this.list = new ArrayList<Object>();
	}
	
	public synchronized void push(Object o){	
		this.list.add(o);
		this.counter++;
		this.notify();
	}
	
	public synchronized Object pop(){
		while(this.counter == 0){
			try
			{
				this.wait();
			}
			catch (InterruptedException iex){};
		}
		
		Object o = list.remove(counter - 1);
		this.counter--;
		return o;
	}
	
	@Override
	public void run()
	{
		if(Math.random()>= 0.5)
		{				
			Object o = (int)(Math.random()*100);
			this.push(o);
			System.out.println(Thread.currentThread().getName() 
					+ " ;Pushed: " + o.toString());
		} 
		else
		{
			Object o = this.pop();
			System.out.println(Thread.currentThread().getName() 
					+ " ;Removed: " + o.toString());
		}
	}
}
