package poll.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import poll.model.Poll;

public class SetController implements ActionListener 
{
	private Poll pollRef;
	
	public SetController(Poll pollRef)
	{
		this.pollRef = pollRef;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) 
	{
		JTextField tf = (JTextField)ae.getSource();
		this.pollRef.setNumber(tf.getName(),Integer.parseInt(tf.getText()));
	}
}
