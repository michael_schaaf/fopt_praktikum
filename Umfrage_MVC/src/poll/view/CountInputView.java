package poll.view;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import poll.controller.SetController;
import poll.model.Poll;
import poll.model.PollChangeListener;
import poll.model.PollData;

@SuppressWarnings("serial")
public class CountInputView extends JPanel implements PollChangeListener 
{
	// Attributes **************************************************************
	private List<JTextArea> labels;
	private List<JTextField> textFields;
	private Poll pollRef;
	
	// Constructors ************************************************************
	public CountInputView(Poll pollRef){
		this.setLayout(new GridLayout(0,2));
		
		this.labels = new ArrayList<JTextArea>();
		this.textFields = new ArrayList<JTextField>();
		this.pollRef = pollRef;
		
		this.answerAdded(pollRef.getResults());
		this.numberChanged(pollRef.getResults());
	}
	
	// Public methods **********************************************************
	@Override
	public void numberChanged(PollData data) 
	{
		for (int i = 0; i < data.getAnswers().length; i++) {
			String text = String.format("%s",data.getNumbers()[i]);
			this.textFields.get(i).setText(text);		
		}
	}

	@Override
	public void answerAdded(PollData data) 
	{
		SetController controller = new SetController(this.pollRef);
		
		for (int i = 0; i < data.getAnswers().length; i++) 
		{
			// Text-Felder erzeugen und in Liste hinzufügen!
			JTextArea t = new JTextArea();
			t.setName(data.getAnswers()[i]);
			t.setText(data.getAnswers()[i]);
			this.add(t);
			
			JTextField tf = new JTextField();
			tf.setName(data.getAnswers()[i]);
			tf.addActionListener(controller);
			this.add(tf);
			
			// Add elements to lists for later update
			this.labels.add(t);
			this.textFields.add(tf);
		}
	}
}
