package poll.view;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import poll.model.Poll;

@SuppressWarnings("serial")
public class QuestionView extends JPanel
{
	public QuestionView(Poll pollRef)
	{
		this.setLayout(new GridLayout(0,1));
		
		JTextArea question = new JTextArea();
		question.setText(pollRef.getResults().getQuestion());
		this.add(question);
	}
}
