package poll.view;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import poll.controller.IncrementController;
import poll.model.Poll;
import poll.model.PollChangeListener;
import poll.model.PollData;

@SuppressWarnings("serial")
public class PercentageView extends JPanel implements PollChangeListener 
{
	// Attributes **************************************************************
	private List<JTextArea> textList;
	private Poll pollRef;
	
	// Constructors ************************************************************
	public PercentageView(Poll pollRef){
		this.setLayout(new GridLayout(0,2));
		
		this.textList = new ArrayList<JTextArea>();
		this.pollRef = pollRef;
		
		this.answerAdded(pollRef.getResults());
		this.numberChanged(pollRef.getResults());
	}
	
	// Public methods **********************************************************
	@Override
	public void numberChanged(PollData data) 
	{
		for (int i = 0; i < data.getAnswers().length; i++) 
		{
			String text = String.format("%s : %s von %s (%s)", 
					data.getAnswers()[i],data.getNumbers()[i],
					data.getTotal(),data.getPercentages()[i]);
			
			this.textList.get(i).setText(text);		
		}
	}

	@Override
	public void answerAdded(PollData data) 
	{
		IncrementController controller = new IncrementController(this.pollRef);
		
		for (int i = 0; i < data.getAnswers().length; i++) 
		{
			// Text-Felder erzeugen und in Liste hinzuf�gen!
			JTextArea t = new JTextArea();
			this.textList.add(t);
			this.add(t);
			
			// Button erzeugen, benennen und ActionListener abbonieren
			JButton b = new JButton("Erh�hen");
			b.setName(data.getAnswers()[i]);
			b.addActionListener(controller);
			this.add(b);
		}
	}	
}
