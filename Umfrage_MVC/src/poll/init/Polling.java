package poll.init;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import poll.model.Poll;
import poll.model.PollImpl;
import poll.view.CountInputView;
import poll.view.PercentageView;
import poll.view.QuestionView;

@SuppressWarnings("serial")
public class Polling extends JFrame
{
	public Polling(String title, String[]args)
	{
		super(title); 
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		// Add question and answers to the model
		if (args.length < 2){
			throw new IllegalArgumentException();
		}

		Poll poll = new PollImpl(args[0]);
		for (int i = 1 ; i < args.length;i++) 
		{
			poll.addAnswer(args[i]);
		}

		// Create all views and add to model as listener
		QuestionView questView = new QuestionView(poll);	
		
		PercentageView percView = new PercentageView(poll);
		poll.addListener(percView);
		
		CountInputView countView = new CountInputView(poll);
		poll.addListener(countView);
		
		// Add all created views to the main panel
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		
		this.add(main);
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 2;
		c.weightx = 0.0;
		c.ipady = 50;
		c.insets = new Insets(10,10,10,10);
		c.gridx = 0;
		c.gridy = 0;
		main.add(questView,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 1.0;
		c.ipady = 250;
		c.insets = new Insets(10,10,10,10);
		c.gridx = 0;
		c.gridy = 1;
		main.add(percView,c);
		
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.ipady = 250;
		c.insets = new Insets(10,10,10,10);
		c.gridx = 1;
		c.gridy = 1;
		main.add(countView,c);	
		
		this.setSize(600,300);
		this.setVisible(true);
	}
	
	public static void main(String[] args) 
	{		
		new Polling("Polling",args);
	}
}
