package poll.model;

import java.util.List;

public class PollData 
{
	// Attributes *****************************************************
	private String question;
	private String[] answers;
    private int[] numbers;
    private int total, max;
    private double[] percentages; 

    // Constructors ***************************************************
    public PollData(List<PollResult> results, String question) {
    	this.question = question;
        this.createData(results);
    }

    // Public Methods *************************************************
	public String[] getAnswers() {
		return answers;
	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}

	public String getQuestion() {
		return question;
	}
	
	public int getTotal() {
		return total;
	}

	public int getMax() {
		return max;
	}

	public double[] getPercentages() {
		return percentages;
	}
	
	// Private methods *************************************************
	private void createData(List<PollResult> results)
	{
		this.answers = new String[results.size()];
    	this.numbers = new int[results.size()];
    	this.percentages = new double[results.size()];
    	
    	for (PollResult pollResult : results) 
    	{
    		int count = pollResult.getCount();
    		total += count;
    		
    		if (count > max) {
				max = pollResult.getCount();
			}
		}
    	
		for (int i = 0; i < results.size(); i++) 
		{
			int count = results.get(i).getCount();
			
			this.answers[i] = results.get(i).getAnswer();
			this.numbers[i] = results.get(i).getCount();
			
			if (this.total > 0){
				this.percentages[i] = Math.round((count / (double)this.total)*100);
			}
		}
	}
}
