package poll.model;

public class PollResult implements Comparable<PollResult>
{
	// Attributes *****************************************************
    private int count;
    private String answer;
    
    // Constructors ***************************************************
    public PollResult(String answer){
    	this.answer = answer;
    	this.count = 0;
    }
    
    // Public Methods *************************************************
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public String getAnswer() {
		return answer;
	}

	@Override
    public boolean equals(Object o)
    {
        if(o.getClass().equals(this.getClass()))
        {
            PollResult result = (PollResult) o; 
            if (this.getAnswer().equals(result.getAnswer()))
            {
                return true; 
            }
        }
        
        return false;
    }

    @Override
    public int compareTo(PollResult result)
    {      
        return result.answer.compareTo(this.answer);
    } 
}
