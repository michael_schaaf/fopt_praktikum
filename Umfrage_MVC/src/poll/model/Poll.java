package poll.model;

public interface Poll 
{
    public abstract void addAnswer(String answer);

    public abstract void addListener(PollChangeListener l);

    public abstract PollData getResults();

    public abstract void incrementNumber(String answer);

    /* Nicht unbedingt ben�tigt!
    public abstract void incrementNumber(int index);

    public abstract void removeListener(PollChangeListener l);

    public abstract void setNumber(int index,int number);
    */
    
    public abstract void setNumber(String answer, int number);
}
