package poll.model;

import java.util.ArrayList;
import java.util.List;

public class PollImpl implements Poll 
{
	// Attributes *****************************************************
    private List<PollResult> results;
    private List<PollChangeListener> listener;
    private String question;

    // Constructors ***************************************************
    public PollImpl(String question)
    {
    	this.results = new ArrayList<PollResult>();
    	this.listener = new ArrayList<PollChangeListener>();
    	this.question = question;
    }
    
    // Public Methods *************************************************
	@Override
	public void addAnswer(String answer) 
	{
		if (answer.equals(null)){
			throw new IllegalArgumentException("answer is null!");
		}
		
		PollResult result = new PollResult(answer);
		if (!this.results.contains(result)){
			results.add(result);
		}
		
		this.notifyListenersAnswerAdded();
	}

	@Override
	public void addListener(PollChangeListener l) 
	{
		if (!this.listener.contains(l)){
			this.listener.add(l);
		}
	}

	@Override
	public PollData getResults() 
	{
		return new PollData(this.results, question);
	}

	@Override
	public void incrementNumber(String answer) {
		if (answer.equals(null)){
			throw new IllegalArgumentException("answer is null!");
		}
		
		PollResult dummy = new PollResult(answer);
		if (this.results.contains(dummy))
		{
			// Get the index 
			int index = this.results.indexOf(dummy);
			
			// Increment number
			int count = this.results.get(index).getCount();
			this.results.get(index).setCount(count + 1);
		}		
		
		this.notifyListenersNumberChanged();
	}

	@Override
	public void setNumber(String answer, int number) {
		if (answer.equals(null))
		{
			throw new IllegalArgumentException("answer is null!");
		}
		
		if (number < 0)
		{
			throw new IllegalArgumentException("number is negative");
		}
		
		PollResult dummy = new PollResult(answer);
		if (this.results.contains(dummy))
		{
			// Get the index 
			int index = this.results.indexOf(dummy);
			this.results.get(index).setCount(number);
		}
		
		this.notifyListenersNumberChanged();
	}
	
	/* Nicht unbedingt ben�tigt!
	public void incrementNumber(int index) 
	{
		if (index < 0 || index >= this.results.size())
		{
			throw new IllegalArgumentException("index out of Range");
		}
		
		int count = this.results.get(index).getCount() + 1;
		this.results.get(index).setCount(count);
		
		this.notifyListenersNumberChanged();
	}

	public void removeListener(PollChangeListener l) 
	{
		this.listener.remove(l);
	}

	public void setNumber(int index, int number) 
	{
		if (index < 0 || index >= this.results.size())
		{
			throw new IllegalArgumentException("index out of Range");
		}
		
		if (number < 0)
		{
			throw new IllegalArgumentException("number is negative");
		}
		
		this.results.get(index).setCount(number);
		
		this.notifyListenersNumberChanged();
	}
	*/

	// Private methods *************************************************
	private void notifyListenersAnswerAdded()
	{
		PollData data = this.getResults();
		
		for (PollChangeListener listener : this.listener){
			listener.answerAdded(data);
		}
	}
	
	private void notifyListenersNumberChanged()
	{
		PollData data = this.getResults();
		
		for (PollChangeListener listener : this.listener) {
			listener.numberChanged(data);
		}
	}
}
