package poll.model;

public interface PollChangeListener 
{
    void numberChanged(PollData data);

    void answerAdded(PollData data);
}
