package poll.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JButton;

import poll.model.Poll;

public class IncrementController implements ActionListener 
{
	private Poll pollRef;
	
	public IncrementController(Poll pollRef)
	{
		this.pollRef = pollRef;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) 
	{
		JButton button = (JButton)ae.getSource();
		final String name = button.getName();
		
		new Thread(()-> { 
			try{
				this.pollRef.incrementNumber(name);} 
			catch (RemoteException rex){};
		}).start();
	}
}