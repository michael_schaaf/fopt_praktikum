package poll.view;

import java.awt.GridLayout;
import java.rmi.RemoteException;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import poll.model.Poll;

@SuppressWarnings("serial")
public class QuestionView extends JPanel
{
	public QuestionView(Poll pollRef)
	{		
		this.setLayout(new GridLayout(0,1));
		
		try
		{
			JTextArea question = new JTextArea();
			question.setText(pollRef.getResults().getQuestion());
			this.add(question);
		}
		catch (RemoteException rex){};
	}
}
