package poll.view;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import poll.controller.IncrementController;
import poll.model.Poll;
import poll.model.PollChangeListener;
import poll.model.PollData;

@SuppressWarnings("serial")
public class PercentageView extends JPanel implements PollChangeListener 
{
	// Attributes **************************************************************
	private List<JTextArea> textList;
	private Poll pollRef;
	
	// Constructors ************************************************************
	public PercentageView(Poll pollRef) throws RemoteException
	{
		UnicastRemoteObject.exportObject(this,0);
		
		this.setLayout(new GridLayout(0,2));
		
		this.textList = new ArrayList<JTextArea>();
		this.pollRef = pollRef;
		
		try
		{
			this.answerAdded(pollRef.getResults());
			this.numberChanged(pollRef.getResults());
		}
		catch (RemoteException rex){};
	}
	
	// Public methods **********************************************************
	@Override
	public void numberChanged(PollData data) 
	{
		for (int i = 0; i < data.getAnswers().length; i++) 
		{
			final String text = String.format("%s : %s von %s (%s)", 
					data.getAnswers()[i],data.getNumbers()[i],
					data.getTotal(),data.getPercentages()[i]);
			final int index = i;
			
			EventQueue.invokeLater(
					() -> this.textList.get(index).setText(text));
		}
	}	

	@Override
	public void answerAdded(PollData data) 
	{
		IncrementController controller = new IncrementController(this.pollRef);
		
		for (int i = 0; i < data.getAnswers().length; i++) 
		{
			// Text-Felder erzeugen und in Liste hinzuf�gen!
			JTextArea t = new JTextArea();
			
			// Button erzeugen, benennen und ActionListener abbonieren
			JButton b = new JButton("Erh�hen");
			b.setName(data.getAnswers()[i]);
			b.addActionListener(controller);
			
			EventQueue.invokeLater(() -> {
				this.add(t);
				this.add(b);
				this.textList.add(t);
			});
		}
	}	
}
