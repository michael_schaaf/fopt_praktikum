package poll.client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.rmi.Naming;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import poll.model.Poll;
import poll.view.CountInputView;
import poll.view.PercentageView;
import poll.view.QuestionView;

@SuppressWarnings("serial")
public class PollClient extends JFrame
{
	public PollClient(String title, String[]args)
	{
		super(title); 
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		try
		{
			Poll poll = (Poll)Naming.lookup("PollModel");
	
			// Create all views and add to model as listener
			QuestionView questView = new QuestionView(poll);	
			
			PercentageView percView = new PercentageView(poll);
			poll.addListener(percView);
			
			CountInputView countView = new CountInputView(poll);
			poll.addListener(countView);
			
			// Add all created views to the main panel
			JPanel main = new JPanel();
			main.setLayout(new GridBagLayout());
			
			this.add(main);
			GridBagConstraints c = new GridBagConstraints();
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridwidth = 2;
			c.weightx = 0.0;
			c.ipady = 50;
			c.insets = new Insets(10,10,10,10);
			c.gridx = 0;
			c.gridy = 0;
			main.add(questView,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridwidth = 1;
			c.weightx = 0.5;
			c.weighty = 1.0;
			c.ipady = 250;
			c.insets = new Insets(10,10,10,10);
			c.gridx = 0;
			c.gridy = 1;
			main.add(percView,c);
			
			c.gridwidth = 1;
			c.weightx = 0.5;
			c.ipady = 250;
			c.insets = new Insets(10,10,10,10);
			c.gridx = 1;
			c.gridy = 1;
			main.add(countView,c);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		this.setSize(600,300);
		this.setVisible(true);
	}
	
	public static void main(String[] args) 
	{		
		new PollClient("Polling",args);
	}
}
