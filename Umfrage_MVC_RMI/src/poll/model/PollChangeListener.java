package poll.model;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PollChangeListener extends Remote
{
    void numberChanged(PollData data) throws RemoteException;

    void answerAdded(PollData data) throws RemoteException;
}
