package poll.model;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("serial")
public class PollImpl extends UnicastRemoteObject implements Poll, Serializable 
{
	// Attributes *****************************************************
    private List<PollResult> results;
    private List<PollChangeListener> listener;
    private String question;

    // Constructors ***************************************************
    public PollImpl(String question) throws RemoteException
    {
    	this.results = new ArrayList<PollResult>();
    	this.listener = new ArrayList<PollChangeListener>();
    	this.question = question;
    }
    
    // Public Methods *************************************************
	@Override
	public synchronized void addAnswer(String answer) throws RemoteException
	{
		if (answer.equals(null)){
			throw new IllegalArgumentException("answer is null!");
		}
		
		PollResult result = new PollResult(answer);
		if (!this.results.contains(result)){
			results.add(result);
		}
		
		this.notifyListenersAnswerAdded();
	}

	@Override
	public synchronized void addListener(PollChangeListener l) throws RemoteException
	{
		if (!this.listener.contains(l)){
			this.listener.add(l);
		}
	}

	@Override
	public synchronized PollData getResults() throws RemoteException
	{
		return new PollData(this.results, question);
	}

	@Override
	public synchronized void incrementNumber(String answer) throws RemoteException
	{
		if (answer.equals(null)){
			throw new IllegalArgumentException("answer is null!");
		}
		
		PollResult dummy = new PollResult(answer);
		if (this.results.contains(dummy))
		{
			// Get the index 
			int index = this.results.indexOf(dummy);
			
			// Increment number
			int count = this.results.get(index).getCount();
			this.results.get(index).setCount(count + 1);
		}		
		
		this.notifyListenersNumberChanged();
	}

	@Override
	public synchronized void setNumber(String answer, int number) throws RemoteException
	{
		if (answer.equals(null))
		{
			throw new IllegalArgumentException("answer is null!");
		}
		
		if (number < 0)
		{
			throw new IllegalArgumentException("number is negative");
		}
		
		PollResult dummy = new PollResult(answer);
		if (this.results.contains(dummy))
		{
			// Get the index 
			int index = this.results.indexOf(dummy);
			this.results.get(index).setCount(number);
		}
		
		this.notifyListenersNumberChanged();
	}
	
	/* Nicht unbedingt ben�tigt!
	public void incrementNumber(int index) 
	{
		if (index < 0 || index >= this.results.size())
		{
			throw new IllegalArgumentException("index out of Range");
		}
		
		int count = this.results.get(index).getCount() + 1;
		this.results.get(index).setCount(count);
		
		this.notifyListenersNumberChanged();
	}

	public void removeListener(PollChangeListener l) 
	{
		this.listener.remove(l);
	}

	public void setNumber(int index, int number) 
	{
		if (index < 0 || index >= this.results.size())
		{
			throw new IllegalArgumentException("index out of Range");
		}
		
		if (number < 0)
		{
			throw new IllegalArgumentException("number is negative");
		}
		
		this.results.get(index).setCount(number);
		
		this.notifyListenersNumberChanged();
	}
	*/

	// Private methods *************************************************
	@SuppressWarnings("rawtypes")
	private void notifyListenersAnswerAdded()
	{
		PollData data = new PollData(this.results, question);
		
		for (Iterator it = listener.iterator(); it.hasNext();) 
		{
			PollChangeListener listener = (PollChangeListener) it.next();
			try
			{
				listener.answerAdded(data);
			}
			catch (RemoteException rex)
			{
				it.remove();
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void notifyListenersNumberChanged()
	{
		PollData data = new PollData(this.results, question);
		
		for (Iterator it = listener.iterator(); it.hasNext();) 
		{
			PollChangeListener listener = (PollChangeListener) it.next();
			try
			{
				listener.numberChanged(data);
			}
			catch (RemoteException rex)
			{
				it.remove();
			}
		}
		
		/*
		for (PollChangeListener listener : this.listener) {
			try{
			listener.numberChanged(data);
			}catch (RemoteException rex){};
		}
		*/
	}
}
