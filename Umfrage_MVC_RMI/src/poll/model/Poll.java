package poll.model;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Poll extends Remote
{
    public abstract void addAnswer(String answer) throws RemoteException;

    public abstract void addListener(PollChangeListener l) throws RemoteException;

    public abstract PollData getResults() throws RemoteException;

    public abstract void incrementNumber(String answer) throws RemoteException;

    /* Nicht unbedingt ben�tigt!
    public abstract void incrementNumber(int index);

    public abstract void removeListener(PollChangeListener l);

    public abstract void setNumber(int index,int number);
    */
    
    public abstract void setNumber(String answer, int number) throws RemoteException;
}
