package aufgabe2;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Display extends JFrame
{
	public Display()
	{
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		Shape shape = new Shape();
		this.add(shape,BorderLayout.CENTER);
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(2,2));
		
		ButtonGroup groupShape = new ButtonGroup();
		JRadioButton rb1 = new JRadioButton("Rechteck", true);
		JRadioButton rb2 = new JRadioButton("Elipse");
		groupShape.add(rb1);
		groupShape.add(rb2);
		
		ButtonGroup groupFill = new ButtonGroup();
		JRadioButton rb3 = new JRadioButton("leer", true);
		JRadioButton rb4 = new JRadioButton("gef�llt");
		groupFill.add(rb3);
		groupFill.add(rb4);	
		
		p.add(rb1);
		p.add(rb3);
		p.add(rb2);
		p.add(rb4);
		
		this.add(p,BorderLayout.SOUTH);
		
		this.setSize(300,300);
		this.setVisible(true);
	}
}

class Shape extends JPanel
{
	private boolean isFilled;
	private boolean isRectangle;
	
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		
		int x = this.getWidth()- 12;
		int y = this.getHeight()- 12;

		if(!isRectangle)
		{
			if (isFilled) 
			{
				g.fillOval(6, 6, x, y);
			}
			g.drawOval(6, 6, x, y);
		}
		else
		{
			if (isFilled) 
			{
				g.fillRect(6, 6, x, y);
			}
			g.drawRect(6, 6, x, y);
		}
		
	}
}
