package praktikum;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
/*
Aufgabe: Ampel

In dieser Aufgabe sollen Sie die Funktionsweise einer Ampel und deren Nutzung nachahmen.

a) Schreiben Sie zun�chst eine Klasse Ampel mit zwei Methoden zum Setzen der Ampel auf rot bzw. gr�n 
(die Zwischenphase gelb spielt keine Rolle � Sie k�nnen von diesem Zustand abstrahieren)! 
Eine dritte Methode mit dem Namen passieren soll das Vorbeifahren eines Fahrzeugs an dieser Ampel nachbilden: 
Ist die Ampel rot, so wird der aufrufende Thread angehalten, und zwar so lange, bis die Ampel gr�n wird. 
Ist die Ampel dagegen gr�n, so kann der Thread sofort aus der Methode zur�ckkehren, ohne den Zustand der Ampel zu ver�ndern. 
Verwenden Sie wait, notify und notifyAll nur an den unbedingt n�tigen Stellen!

b) Schreiben Sie nun eine Klasse Auto (abgeleitet aus Thread)! 
Im Konstruktor wird eine Referenz auf ein Feld von Ampeln �bergeben, wobei diese Referenz in einem entsprechenden Attribut der Klasse Auto gespeichert wird. 
In der Run-Methode werden alle Ampeln des Feldes passiert, und zwar in einer Endlosschleife 
(d.h. nach dem Passieren der letzten Ampel des Feldes wird wieder die erste Ampel im Feld passiert).

c) Schreiben Sie eine weitere Thread-Klasse zur Steuerung der Ampeln und probieren Sie Ihre Threads und Ampeln aus!

Hinweis: 
Sie m�ssen nicht wie in der Realit�t nachahmen, dass beim Gr�nwerden der Ampel die Autos der Reihe nach losfahren. 
Stellen Sie sich der Einfachheit halber eine Ampel vor, bei der sich alle wartenden Autos nebeneinander aufstellen (wie in Italien)! 
Sobald die Ampel gr�n wird, kann jedes Auto unabh�ngig von allen anderen Autos losfahren. 
*/

public class Ampel
{
	/**
	 * Ist das flag true, so ist die Ampel rot! 
	 * Ist das flag false so ist die Ampel gr�n.
	 */
	private boolean istRot;	
	private ReentrantLock lock;
	private Condition anAmpel;
	private int waitingCount;
	
	public Ampel(){
		this.lock = new ReentrantLock();
		this.anAmpel = lock.newCondition();
	}
	
	public void rot()
	{
		lock.lock();
		try
		{
			this.istRot = true;
		} 
		finally 
		{
			lock.unlock();
		}	
	}
	
	public void gruen(){
		lock.lock();
		try
		{
			this.istRot = false;
			if(this.waitingCount > 0)
			{
				this.anAmpel.signalAll();
			}
		} 
		finally 
		{
			lock.unlock();
		}	
	}
	
	public void passieren(){
		lock.lock();
		try
		{
			waitingCount ++;
			while(this.istRot){
				this.anAmpel.awaitUninterruptibly();
				System.out.println(Thread.currentThread().getName() + ": Warten an Ampel!");
			}
			waitingCount --;
		} 
		finally 
		{
			lock.unlock();
		}	
	}
}
