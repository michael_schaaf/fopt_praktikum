package poll.client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.JTextField;

import poll.server.model.interfaces.IPoll;

public class SetController implements ActionListener 
{
	private IPoll pollRef;
	
	public SetController(IPoll pollRef)
	{
		this.pollRef = pollRef;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) 
	{
		JTextField tf = (JTextField)ae.getSource();
		final String name = tf.getName();
		final int number = Integer.parseInt(tf.getText());
		
		new Thread(()-> { 
			try{
				this.pollRef.setNumber(name, number);} 
			catch (RemoteException rex){};
		}).start();
	}
}
