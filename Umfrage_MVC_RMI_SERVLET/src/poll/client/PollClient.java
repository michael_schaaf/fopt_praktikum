package poll.client;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.rmi.Naming;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import poll.client.view.CountInputView;
import poll.client.view.PercentageView;
import poll.client.view.QuestionView;
import poll.server.model.interfaces.IPoll;

@SuppressWarnings("serial")
public class PollClient extends JFrame
{
	public PollClient(String title, String[]args)
	{
		super(title); 
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		try
		{
			IPoll poll = (IPoll)Naming.lookup("rmi://localhost/PollModel");
	
			JPanel main = new JPanel();
			main.setLayout(new GridLayout(0,2));
			
			// Create all views and add to model as listener
			QuestionView questView = new QuestionView(poll);
			this.add(questView, BorderLayout.NORTH);
			
			PercentageView percView = new PercentageView(poll);
			poll.addListener(percView);
			
			CountInputView countView = new CountInputView(poll);
			poll.addListener(countView);
			
			main.add(percView);
			main.add(countView);
			this.add(main, BorderLayout.CENTER);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		this.setSize(600,300);
		this.setVisible(true);
	}
	
	public static void main(String[] args) 
	{		
		new PollClient("Polling",args);
	}
}
