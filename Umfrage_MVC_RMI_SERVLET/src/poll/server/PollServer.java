package poll.server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import poll.server.model.PollImpl;
import poll.server.model.interfaces.IPoll;

public class PollServer 
{
	public PollServer(String[] args)
	{
		// Add question and answers to the model
		if (args.length < 2){
			throw new IllegalArgumentException();
		}

		try
		{
			IPoll poll = new PollImpl(args[0]);
			for (int i = 1 ; i < args.length;i++) 
			{
				poll.addAnswer(args[i]);
			}
					
			Registry rmiReg = LocateRegistry.createRegistry(1099);
			rmiReg.rebind("PollModel",poll);
			System.out.println("Server ist gestartet!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{
		new PollServer(args);
	}
}
