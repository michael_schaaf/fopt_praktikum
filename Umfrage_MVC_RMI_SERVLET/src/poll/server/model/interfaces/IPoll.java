package poll.server.model.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import poll.server.model.PollData;

public interface IPoll extends Remote
{
    public abstract void addAnswer(String answer) throws RemoteException;

    public abstract void addListener(IPollChangeListener l) throws RemoteException;

    public abstract PollData getResults() throws RemoteException;

    public abstract void incrementNumber(String answer) throws RemoteException;
    
    public abstract void setNumber(String answer, int number) throws RemoteException;
}
