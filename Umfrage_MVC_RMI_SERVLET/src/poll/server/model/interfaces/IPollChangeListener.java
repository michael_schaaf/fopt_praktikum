package poll.server.model.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import poll.server.model.PollData;

public interface IPollChangeListener extends Remote
{
    void numberChanged(PollData data) throws RemoteException;

    void answerAdded(PollData data) throws RemoteException;
}
