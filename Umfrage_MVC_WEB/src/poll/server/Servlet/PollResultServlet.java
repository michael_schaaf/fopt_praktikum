package poll.server.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import poll.server.model.PollData;
import poll.server.model.interfaces.IPoll;

@WebServlet("/PollResult")
public class PollResultServlet extends HttpServlet 
{   
	private static final long serialVersionUID = -8562620567035173192L;
	private IPoll poll;
	
    public PollResultServlet() 
    {
    	try
		{
    		// Referenz auf RMI Interface f�r Model holen
			poll = (IPoll)Naming.lookup("rmi://localhost/PollModel");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
    }

	protected void doGet(
			HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		this.sendHtmlResponse(out);
	}

	protected void doPost(
			HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException 
	{
		this.doGet(request, response);
	}
	
	private void sendHtmlResponse(PrintWriter out) throws RemoteException
	{
		// Aufruf der Daten �ber RMI Interface
		PollData data = this.poll.getResults();
						
		out.println("<html><head>");
		out.println("<title>Umfrage</title>");
		out.println("<style>table, td, th { border: 1px solid; }</style>");
		out.println("</head>");
		out.println("<body>");
		out.println("<form method=\"get\" action=\"PollResult\">");
		out.println(String.format("<H2>Momentaner Stand der Umfrage: %s</H2>", data.getQuestion()));
		out.println("<table>");
		out.println("<tr><th>Answer<th>Number<th>%</tr>");
		
		double percentSum = 0.0;
		for (int i = 0; i < data.getAnswers().length; i++) 
		{
			String answer = data.getAnswers()[i];
			int number = data.getNumbers()[i];
			double percent = data.getPercentages()[i];
			percentSum += percent;
			
			out.println(String.format("<tr><td>%s<td>%s<td>%s %%</tr>", 
					answer,number,percent));
		}
		out.println(String.format("<tr><td>Summe<td>%s<td>%s %%</tr>",data.getTotal(),percentSum));
		out.println("</table>");
		out.println("<p></p>");
		out.println("<a href=\"http://localhost:8080/Umfrage_MVC_WEB/Poll\">Zur Abstimmung</a>");
		out.println("</body><html>");
	}
}
