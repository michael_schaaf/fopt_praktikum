package poll.server.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import poll.server.model.PollData;
import poll.server.model.interfaces.IPoll;

@WebServlet("/Poll")
public class PollServlet extends HttpServlet 
{   
	private static final long serialVersionUID = -5848124053096669040L;
	private IPoll poll;
	
    public PollServlet() 
    {
    	try
		{
    		// Referenz auf RMI INterface f�r Model holen
			poll = (IPoll)Naming.lookup("rmi://localhost/PollModel");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
    }

	protected void doGet(
			HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		this.sendHtmlResponse(out);
	}

	protected void doPost(
			HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException 
	{
		this.doGet(request, response);
	}
	
	private void sendHtmlResponse(PrintWriter out) throws RemoteException
	{
		// Aufruf der Daten �ber RMI Interface
		PollData data = this.poll.getResults();
						
		out.println("<html><head>");
		out.println("<title>Umfrage</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<form method=\"get\" action=\"PollDone\">");
		out.println(String.format("<H2>Umfrage: %s</H2>", data.getQuestion()));
		
		// Generieren der Buttons
		for (int i = 0; i < data.getAnswers().length; i++) {
			String answer = data.getAnswers()[i];
			out.println(String.format(
					"<p><input type=\"radio\" name=\"selectedAnswer\" value=\"%s\">%s</p>", 
					answer,answer));
		}
		
		out.println("<input type=\"submit\" value=\"Abstimmen\"></form>");
		out.println("<p></p>");
		out.println("<a href=\"http://localhost:8080/Umfrage_MVC_WEB/PollResult\">Bisheriges Ergebnis der Umfrage</a>");
		out.println("</body><html>");
	}
}
