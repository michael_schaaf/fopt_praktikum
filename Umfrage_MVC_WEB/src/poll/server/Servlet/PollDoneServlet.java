package poll.server.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import poll.server.model.PollData;
import poll.server.model.interfaces.IPoll;

@WebServlet("/PollDone")
public class PollDoneServlet extends HttpServlet 
{   
	private static final long serialVersionUID = -8562620567035173192L;
	private IPoll poll;
	
    public PollDoneServlet() 
    {
    	try
		{
    		// Referenz auf RMI Interface f�r Model holen
			poll = (IPoll)Naming.lookup("rmi://localhost/PollModel");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
    }

	protected void doGet(
			HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		Object para = request.getParameter("selectedAnswer");
		if (para != null) {
			String answer = request.getParameter("selectedAnswer").toString();
			poll.incrementNumber(answer);
		}

		this.sendHtmlResponse(out);
	}

	protected void doPost(
			HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException 
	{
		this.doGet(request, response);
	}
	
	private void sendHtmlResponse(PrintWriter out) throws RemoteException
	{
		// Aufruf der Daten �ber RMI Interface
		PollData data = this.poll.getResults();
						
		out.println("<html><head>");
		out.println("<title>Umfrage</title>");
		out.println("</head>");
		out.println("<body>");
		out.println(String.format("<H2>Umfrage: %s</H2>", data.getQuestion()));
		out.println("<p>Vielen Dank f�r Ihre Teilnahme an der Umfrage</p>");
		out.println("<p></p>");
		out.println("<a href=\"http://localhost:8080/Umfrage_MVC_WEB/PollResult\">Bisheriges Ergebnis der Umfrage</a>");
		out.println("</body><html>");
	}
}
