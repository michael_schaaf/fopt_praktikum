package praktikum;

public class AmpelTest {

	public static void main(String[] args) {
		Ampel[] ampeln = new Ampel[10];
		
		for (int i = 0; i < ampeln.length; i++) {
			ampeln[i] = new Ampel();
			AmpelSchaltung as = new AmpelSchaltung(ampeln[i]);
			as.start();
		}

		Auto auto1 = new Auto(ampeln);
		auto1.setName("Porsche");
		auto1.start();
		
		try{
			Thread.sleep(250);
		} catch (InterruptedException iex){};
		
		Auto auto2 = new Auto(ampeln);
		auto2.setName("Ford");
		auto2.start();
		
		try{
			Thread.sleep(250);
		} catch (InterruptedException iex){}; 
		
		Auto auto3 = new Auto(ampeln);
		auto3.setName("Volkswagen");
		auto3.start();
	}
}
