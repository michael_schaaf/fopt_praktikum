package praktikum;

public class AmpelSchaltung extends Thread
{
	private Ampel ampel;
	
	public AmpelSchaltung(Ampel a)
	{
		this.ampel = a;	
	}
	
	public void run(){
		while(true)
		{
				this.ampel.rot();
				
				try{
					Thread.sleep(250);
				} catch (InterruptedException iex){};
				
				this.ampel.gruen();
		}
	}
}
