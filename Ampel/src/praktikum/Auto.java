package praktikum;

public class Auto extends Thread
{
	private Ampel[] ampeln;
	
	public Auto(Ampel[] a)
	{
		this.ampeln = a;
	}
	
	@Override
	public void run(){		
		while(true)
		{
			for(int i = 0; i < this.ampeln.length; i++)
			{
				this.ampeln[i].passieren();
				System.out.println(this.getName() + ": Ampel" + i + " passiert!");
			}
		}
	}
}
