package udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;


public class StaticServer
{
	public StaticServer(){
		try
		{
			DatagramSocket socket = new DatagramSocket(1250);
			for(int i = 0; i < 20; i++)
			{
				new Listener(socket).start();
			}
		} 
		catch (SocketException e){}
	}
	
	public static void main(String[] args){
		new StaticServer();
	}
}

class Listener extends Thread
{
	DatagramPacket packet = null;
	DatagramSocket socket;
	byte[] buffer = new byte[1000];
	
	public Listener(DatagramSocket socket){
		this.socket = socket;
	}
	
	public byte[] process(byte[] data, int length)
	{
		byte[] answerByteArray = data;
		return answerByteArray;
	}
	
	@Override
	public void run() 
	{
		while (true) 
		{
            try 
            {
            	socket.receive(packet);
				buffer = this.process(packet.getData(), buffer.length);
				
				DatagramPacket out = new DatagramPacket(
						buffer, buffer.length, packet.getAddress(), packet.getPort());
				socket.send(out);
			} 
            catch (IOException e) {}
		}
	}
}
